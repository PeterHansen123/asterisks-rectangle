import java.util.Scanner;

public class Rectangle {
    public static void rectangle(int size) {
        for(int i = 1; i <= size; i++) {
            for(int j = 1; j <= size; j++) {
                if(i == 1 || i == size || j == 1 || j == size) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
        System.out.println();
    }
}

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter size");
        int size = input.nextInt();
        rectangle(size);
    }
}

// Credit: https://computecodes.com/java/create-hollow-rectangle-using-asterisks-java/