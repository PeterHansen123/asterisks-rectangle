import java.util.Scanner;

public class NewRectangle {
    public static void main(String[] args) {
        int width;
        int height;

        Scanner in = new Scanner(System.in);

        System.out.print("Enter width: ");
        width = in.nextInt();
        System.out.print("Enter height: ");
        height = in.nextInt();

        // Printing the top
        for(int i = 1; i <= width; i++)
            System.out.print("*");
            System.out.println();

        // Printing sides
        for(int j = 1; j <= height - 2; j++) {
            // Left side
            System.out.print("*");
            // Spaces in the middle
            for(int i = 1; i <= width - 2; i++)
                System.out.print(" ");
            // Right side
            System.out.println("*");
        }
        // Printing the bottom
        for(int i = 1; i <= width; i = i + 1)
            System.out.print("*");
        System.out.println();
    }
}
// Credit: https://www.youtube.com/watch?v=jIR-YrFqrpY